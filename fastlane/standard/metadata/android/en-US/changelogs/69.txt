New in 1.7.6
★ Upgrade a lot of dependencies.

New in 1.7.5
★ Update Portuguese (Portugal) and Belarusian translations.
